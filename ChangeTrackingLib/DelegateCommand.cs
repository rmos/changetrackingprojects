﻿using System;
using System.Collections.Specialized;
using System.Windows.Input;

namespace ChangeTracking
{
    public class DelegateCommand : ICommand
    {
        private Func<NotifyCollectionChangedEventArgs> undo;
        private object canUndo;
        private Action reset;
        private object canReset;

        public event EventHandler CanExecuteChanged;

        Action<object> Action { get; set; }

        Func<object, bool> Func { get; set; }

        public DelegateCommand(Action<object> action)
            : this(action, null)
        {
        }

        public DelegateCommand(Action<object> action, Func<object, bool> func)
        {
            this.Action = action;
            this.Func = func;
        }

        public DelegateCommand(Func<NotifyCollectionChangedEventArgs> undo, object canUndo)
        {
            this.undo = undo;
            this.canUndo = canUndo;
        }

        public DelegateCommand(Action reset, object canReset)
        {
            this.reset = reset;
            this.canReset = canReset;
        }

        public bool CanExecute(object parameter)
        {
            if (Func == null)
                return true;
            return Func.Invoke(parameter);
        }

        public void Execute(object parameter)
        {
            Action.Invoke(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            EventHandler h = CanExecuteChanged;
            h?.Invoke(this, EventArgs.Empty);
        }
    }
}
