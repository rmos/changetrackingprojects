﻿namespace ChangeTracking
{
    public struct PropertyValue
    {
        public string PropertyName { get; set; }

        public object Value { get; set; }

        public override string ToString()
        {
            return $"{PropertyName ?? "None"} = {Value ?? "None"}";
        }
    }
}
