﻿using System.ComponentModel;

namespace ChangeTracking
{
    public interface ITrackingObject : INotifyPropertyChanged
    {
        bool IsNew { get; set; }

        bool IsRemoved { get; set; }

        PropertyTrackingContext<INotifyPropertyChanged> ChangeTracker { get; }
    }
}
