﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ChangeTracking
{
    public abstract class ChangeTrackingBase<TChange> : IChangeTracking<TChange>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool isApplyChange;

        public bool IsApplyChange
        {
            get { return isApplyChange; }
            private set
            {

                if (value != isApplyChange)
                {
                    isApplyChange = value;
                    OnPropertyChanged(nameof(IsApplyChange));
                }
            }
        }

        private int changesTrackCount = 100;

        public int ChangesTrackCount
        {
            get { return changesTrackCount; }
            set
            {
                if (value <= 1)
                    throw new ArgumentException("ChangesMaxCount range is from 1 to int.MaxValue");
                if (value != changesTrackCount)
                {
                    changesTrackCount = value;
                    OnPropertyChanged(nameof(ChangesTrackCount));
                }
            }
        }

        private void AdjustChangesCount()
        {
            int count = ChangesList.Count - ChangesTrackCount;
            for (int i = count; i > 0; i--)
                ChangesList.RemoveFirst();
        }

        IList<TChange> OriginalChangesList { get; } = new List<TChange>();

        public IEnumerable<TChange> OriginalChanges { get { return OriginalChangesList; } }

        protected LinkedList<TChange> ChangesList { get; } = new LinkedList<TChange>();

        public IEnumerable<TChange> Changes { get { return ChangesList; } }

        public IEnumerable<TChange> ChangesReversed { get { return Changes.Reverse(); } }

        protected LinkedListNode<TChange> CurrentNode { get; private set; }

        public TChange CurrentChange { get { return CurrentNode == null ? default(TChange) : CurrentNode.Value; } }

        public bool HasChanges { get { return ChangesList.Count > 0 && CurrentNode != null; } }

        public bool HasUndos { get { return CurrentNode != null; } }

        public bool HasRedos { get { return CurrentNode != ChangesList.Last; } }

        private bool isTracking;

        public bool IsTracking
        {
            get { return isTracking; }
            private set
            {
                if (value != isTracking)
                {
                    isTracking = value;
                    OnPropertyChanged(nameof(IsTracking));
                }
            }
        }

        private bool isInitiated;

        public bool IsInitiated
        {
            get { return isInitiated; }
            private set
            {
                if (value != isInitiated)
                {
                    isInitiated = value;
                    OnPropertyChanged(nameof(IsInitiated));
                }
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler h = PropertyChanged;
            h?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected abstract void InitOriginalChangesOverride(IList<TChange> originalChanges);

        protected void InitOriginalChanges()
        {
            IsApplyChange = true;
            OriginalChangesList.Clear();
            InitOriginalChangesOverride(OriginalChangesList);
            IsInitiated = true;
            IsApplyChange = false;
        }

        protected abstract void ApplyOriginalChangesOverride();

        protected void ApplyOriginalChanges()
        {
            IsApplyChange = true;
            ApplyOriginalChangesOverride();
            IsApplyChange = false;
        }

        private void RaiseChangesChange()
        {
            OnPropertyChanged(nameof(Changes));
            OnPropertyChanged(nameof(ChangesReversed));
            OnPropertyChanged(nameof(HasChanges));
            OnPropertyChanged(nameof(HasUndos));
            OnPropertyChanged(nameof(HasRedos));
            OnPropertyChanged(nameof(CurrentChange));
        }

        protected abstract void ClearChangesOverride();

        public void ClearChanges()
        {
            IsApplyChange = true;
            ChangesList.Clear();
            CurrentNode = null;
            ClearChangesOverride();
            IsApplyChange = false;
            RaiseChangesChange();
        }

        public void Reset()
        {
            ClearChanges();
            ApplyOriginalChanges();
        }

        public virtual void AddChange(TChange change)
        {
            if (HasRedos)
            {
                LinkedListNode<TChange> node = (CurrentNode ?? ChangesList.First).Next, node2;
                while (node != null)
                {
                    node2 = node.Next;
                    ChangesList.Remove(node);
                    node = node2;
                }
            }

            CurrentNode = ChangesList.AddLast(change);
            AdjustChangesCount();
            RaiseChangesChange();
        }

        protected abstract void ApplyUndo(LinkedListNode<TChange> change);

        public TChange Undo()
        {
            if (!HasUndos)
                throw new InvalidOperationException("There is no undo changes available.");
            TChange result = CurrentNode.Value;
            IsApplyChange = true;
            ApplyUndo(CurrentNode);
            CurrentNode = CurrentNode.Previous;
            IsApplyChange = false;
            RaiseChangesChange();
            return result;
        }

        protected abstract void ApplyRedo(LinkedListNode<TChange> change);

        public TChange Redo()
        {
            if (!HasRedos)
                throw new InvalidOperationException("There is no redo changes available.");
            TChange result;
            IsApplyChange = true;
            CurrentNode = CurrentNode?.Next ?? ChangesList.First;
            result = CurrentNode.Value;
            ApplyRedo(CurrentNode);
            IsApplyChange = false;
            RaiseChangesChange();
            return result;
        }

        protected abstract void StartTrackingOverride();

        public void StartTracking()
        {
            if (IsTracking)
                throw new NotSupportedException("Tracking is already started.");
            if (!IsInitiated)
                InitOriginalChanges();
            StartTrackingOverride();
            IsTracking = true;
        }

        protected abstract void StopTrackingOverride();

        public void StopTracking()
        {
            if (!IsTracking)
                throw new NotSupportedException("Tracking is already stopped.");
            StopTrackingOverride();
            IsTracking = false;
            IsInitiated = false;
        }

        public void ToggleTracking()
        {
            IsTracking = !IsTracking;
            if (IsTracking)
            {
                StartTracking();
            }
            else
            {
                StopTracking();
            }
        }
    }
}
