﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace ChangeTracking
{
    public abstract class CollectionTracking<TSource, TData> : ChangeTrackingBase<NotifyCollectionChangedEventArgs>
        where TSource : INotifyCollectionChanged, IList<TData>
    {
        public TSource Source { get; private set; }
        
        public CollectionTracking(TSource source)
        {
            Source = source;
        }

        protected override void InitOriginalChangesOverride(IList<NotifyCollectionChangedEventArgs> originalChanges)
        {
            originalChanges.Add(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, Source.ToList()));
        }

        protected override void ApplyOriginalChangesOverride()
        {
            Source.Clear();
            NotifyCollectionChangedEventArgs change = OriginalChanges.First();
            foreach (var obj in change.NewItems.Cast<TData>().ToList())
            {
                Source.Add(obj);
            }
        }

        private void Source_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!IsApplyChange)
            {
                AddChange(e);
            }

            OnSourceCollectionChanged(e);
        }

        protected virtual void OnSourceCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
        }

        protected override void StartTrackingOverride()
        {
            Source.CollectionChanged += Source_CollectionChanged;
        }

        protected override void StopTrackingOverride()
        {
            Source.CollectionChanged -= Source_CollectionChanged;
        }

        protected override void ApplyUndo(LinkedListNode<NotifyCollectionChangedEventArgs> change)
        {
            NotifyCollectionChangedEventArgs args = change.Value;

            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var obj in args.NewItems.Cast<TData>().ToList())
                    {
                        Source.Remove(obj);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    int idx = args.OldStartingIndex;
                    foreach (var obj in args.OldItems.Cast<TData>().ToList())
                    {
                        Source.Insert(idx, obj);
                        idx++;
                    }
                    break;
                case NotifyCollectionChangedAction.Replace:
                    Source.ReplaceItems(args.NewItems.Cast<TData>().ToList());
                    break;
                case NotifyCollectionChangedAction.Move:
                    Source.MoveItem(args.NewStartingIndex, args.OldStartingIndex);
                    break;
                default:
                    break;
            }
        }

        protected override void ApplyRedo(LinkedListNode<NotifyCollectionChangedEventArgs> change)
        {
            NotifyCollectionChangedEventArgs args = change.Value;
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    int idx = args.NewStartingIndex;
                    foreach (var obj in args.NewItems.Cast<TData>().ToList())
                    {
                        Source.Insert(idx, obj);
                        idx++;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var obj in args.OldItems.Cast<TData>().ToList())
                    {
                        Source.Remove(obj);
                    }
                    break;
                case NotifyCollectionChangedAction.Replace:
                    Source.ReplaceItems(args.NewItems.Cast<TData>().ToList());
                    break;
                case NotifyCollectionChangedAction.Move:
                    Source.MoveItem(args.OldStartingIndex, args.NewStartingIndex);
                    break;
                default:
                    break;
            }
        }
    }
}
