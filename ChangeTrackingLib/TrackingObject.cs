﻿using System.ComponentModel;

namespace ChangeTracking
{
    public class TrackingObject : ITrackingObject
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private PropertyTrackingContext<INotifyPropertyChanged> changeTracker;
        public PropertyTrackingContext<INotifyPropertyChanged> ChangeTracker
        {
            get
            {
                if (changeTracker == null)
                    changeTracker = new PropertyTrackingContext<INotifyPropertyChanged>(this);
                return changeTracker;
            }
        }

        private bool isNew;

        public virtual bool IsNew
        {
            get { return isNew; }
            set
            {
                if (value != isNew)
                {
                    isNew = value;
                    OnPropertyChanged(nameof(IsNew));
                }
            }
        }

        private bool isRemoved;

        public virtual bool IsRemoved
        {
            get { return isRemoved; }
            set
            {
                if (value != isRemoved)
                {
                    isRemoved = value;
                    OnPropertyChanged(nameof(IsRemoved));
                }
            }
        }

        public bool IsTracking
        {
            get { return ChangeTracker.IsTracking; }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler h = PropertyChanged;
            h?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void StartTracking()
        {
            if (!ChangeTracker.IsTracking)
                ChangeTracker.StartTracking();
            OnPropertyChanged(nameof(IsTracking));
        }

        public void StopTracking()
        {
            if (ChangeTracker.IsTracking)
                ChangeTracker.StopTracking();
            OnPropertyChanged(nameof(IsTracking));
        }

        public void ToggleTracking()
        {
            ChangeTracker.ToggleTracking();
        }
    }
}