﻿using System;
using System.ComponentModel;

namespace ChangeTracking
{
    public class PropertyTrackingContext<TSource> : PropertyTracking<TSource>
        where TSource : INotifyPropertyChanged
    {
        public event EventHandler<DelegateCommandEventArgs> CommandExecuted;

        public DelegateCommand UndoCommand { get; private set; }

        public DelegateCommand RedoCommand { get; private set; }

        public DelegateCommand ResetCommand { get; private set; }
        
        public PropertyTrackingContext(TSource source) : base(source)
        {
            CreateCommands();
        }

        private void CreateCommands()
        {
            UndoCommand = new DelegateCommand(Undo, CanUndoTracker);
            RedoCommand = new DelegateCommand(Redo, CanRedo);
            ResetCommand = new DelegateCommand(Reset, CanReset);
        }

        protected void OnCommandExecuted(DelegateCommand command)
        {
            EventHandler<DelegateCommandEventArgs> h = CommandExecuted;
            h?.Invoke(this, new DelegateCommandEventArgs(command));
        }

        public void RaiseCommandsCanExecute()
        {
            UndoCommand.RaiseCanExecuteChanged();
            RedoCommand.RaiseCanExecuteChanged();
            ResetCommand.RaiseCanExecuteChanged();
        }

        public override void AddChange(PropertyValue change)
        {
            base.AddChange(change);
            RaiseCommandsCanExecute();
        }

        protected virtual bool CanUndoTracker(object args)
        {
            return HasUndos;
        }

        public void Undo(object args)
        {
            base.Undo();
            RaiseCommandsCanExecute();
            OnCommandExecuted(UndoCommand);
        }

        protected virtual bool CanRedo(object args)
        {
            return HasRedos;
        }

        public void Redo(object args)
        {
            base.Redo();
            RaiseCommandsCanExecute();
            OnCommandExecuted(RedoCommand);
        }

        protected virtual bool CanReset(object args)
        {
            return CanUndoTracker(args) || CanRedo(args);
        }

        private void Reset(object args)
        {
            base.Reset();
            RaiseCommandsCanExecute();
            OnCommandExecuted(ResetCommand);
        }
    }
}
