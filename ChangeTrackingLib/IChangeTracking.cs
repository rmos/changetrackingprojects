﻿using System.Collections.Generic;

namespace ChangeTracking
{
    public interface IChangeTracking<TChange>
    {
        IEnumerable<TChange> OriginalChanges { get; }
        TChange CurrentChange { get; }
        bool HasChanges { get; }
        bool HasUndos { get; }
        bool HasRedos { get; }
        IEnumerable<TChange> Changes { get; }
        int ChangesTrackCount { get; }
        void AddChange(TChange change);
        void ClearChanges();
        void Reset();
        TChange Redo();
        TChange Undo();
        bool IsTracking { get; }
        void StartTracking();
        void StopTracking();
        void ToggleTracking();
    }
}