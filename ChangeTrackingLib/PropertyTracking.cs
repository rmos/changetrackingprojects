﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ChangeTracking
{
    public class PropertyTracking<TSource> : ChangeTrackingBase<PropertyValue>
        where TSource : INotifyPropertyChanged
    {
        public TSource Source { get; private set; }

        public PropertyTracking(TSource source)
        {
            Source = source;
        }

        private void Source_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (IsApplyChange)
                return;
            ChangeTrackingAttribute attr = sender.GetType().GetProperty(e.PropertyName).GetCustomAttribute<ChangeTrackingAttribute>();
            if (attr != null)
            {
                object val = sender.GetType().GetProperty(e.PropertyName).GetValue(sender);
                AddChange(CreateChange(e.PropertyName, val));
            }
        }

        protected override void StartTrackingOverride()
        {
            Source.PropertyChanged += Source_PropertyChanged;
        }

        protected override void StopTrackingOverride()
        {
            Source.PropertyChanged -= Source_PropertyChanged;
        }

        private static PropertyValue CreateChange(string propertyName, object value)
        {
            return new PropertyValue { PropertyName = propertyName, Value = value };
        }

        protected override void InitOriginalChangesOverride(IList<PropertyValue> originalChanges)
        {
            foreach (var p in Source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(o => o.GetSetMethod() != null))
                originalChanges.Add(CreateChange(p.Name, p.GetValue(Source)));
        }

        void ApplyChange(PropertyValue change)
        {
            Source.GetType().GetProperty(change.PropertyName).SetValue(Source, change.Value);
        }

        void ApplyChange(LinkedListNode<PropertyValue> node)
        {
            ApplyChange(node.Value);
        }

        protected override void ApplyOriginalChangesOverride()
        {
            Type t = Source.GetType();
            foreach (PropertyValue pv in OriginalChanges)
                ApplyChange(pv);
        }

        protected override void ApplyUndo(LinkedListNode<PropertyValue> node)
        {
            //search for previous
            LinkedListNode<PropertyValue> previous = node.Previous;
            while (previous != null)
            {
                if (node.Value.PropertyName == previous.Value.PropertyName)
                {
                    break;
                }

                previous = previous.Previous;
            }

            if (previous == null)
            {
                // apply original
                ApplyChange(OriginalChanges.First(o => o.PropertyName == node.Value.PropertyName));
            }
            else
            {
                ApplyChange(previous);
            }
        }

        protected override void ApplyRedo(LinkedListNode<PropertyValue> node)
        {
            ApplyChange(node);
        }

        protected override void ClearChangesOverride()
        {
        }
    }
}
