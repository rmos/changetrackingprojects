﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;

namespace ChangeTracking
{
    public class CollectionTrackingContext<TSource, TData> : CollectionTracking<TSource, TData>
        where TSource : INotifyCollectionChanged, IList<TData>
        where TData : class, ITrackingObject
    {
        public DelegateCommand ClearItemsCommand { get; private set; }

        public DelegateCommand RemoveItemsCommand { get; private set; }

        public DelegateCommand UndoCommand { get; private set; }

        public DelegateCommand RedoCommand { get; private set; }

        public DelegateCommand ResetCommand { get; private set; }

        public DelegateCommand MoveUpItemsCommand { get; private set; }

        public DelegateCommand MoveDownItemsCommand { get; private set; }

        public DelegateCommand MoveTopItemsCommand { get; private set; }

        public DelegateCommand MoveBottomItemsCommand { get; private set; }

        public DelegateCommand ReplaceItemsCommand { get; private set; }

        public DelegateCommand CollectItemsCommand { get; private set; }

        private TData currentItem;

        public TData CurrentItem
        {
            get { return currentItem; }
            set
            {
                if (value != currentItem)
                {
                    currentItem = value;
                    if (currentItem != null && !currentItem.ChangeTracker.IsTracking)
                    {
                        currentItem.ChangeTracker.StartTracking();
                        currentItem.PropertyChanged += CurrentItem_PropertyChanged;
                    }

                    OnPropertyChanged(nameof(CurrentItem));
                    RaiseCommandsCanExecute();
                }
            }
        }

        protected ObservableCollection<TData> RemovedItemsList { get; } = new ObservableCollection<TData>();

        public IEnumerable<TData> RemovedItems
        {
            get { return RemovedItemsList; }
        }

        public CollectionTrackingContext(TSource source) : base(source)
        {
            CreateCommands();
            RemovedItemsList.CollectionChanged += RemovedItemsList_CollectionChanged;
        }

        void CreateCommands()
        {
            ClearItemsCommand = new DelegateCommand(ClearItems, CanClearItems);
            RemoveItemsCommand = new DelegateCommand(RemoveItems, CanRemoveItems);
            UndoCommand = new DelegateCommand(Undo, CanUndo);
            RedoCommand = new DelegateCommand(Redo, CanRedo);
            ResetCommand = new DelegateCommand(Reset, CanReset);
            MoveUpItemsCommand = new DelegateCommand(MoveUpItems, CanMoveUpItems);
            MoveTopItemsCommand = new DelegateCommand(MoveTopItems, CanMoveTopItems);
            MoveDownItemsCommand = new DelegateCommand(MoveDownItems, CanMoveDownItems);
            MoveBottomItemsCommand = new DelegateCommand(MoveBottomItems, CanMoveBottomItems);
            ReplaceItemsCommand = new DelegateCommand(ReplaceItems, CanReplaceItems);
            CollectItemsCommand = new DelegateCommand(CollectItems, CanCollectItems);
        }

        public virtual void RaiseCommandsCanExecute()
        {
            ClearItemsCommand.RaiseCanExecuteChanged();
            RemoveItemsCommand.RaiseCanExecuteChanged();
            UndoCommand.RaiseCanExecuteChanged();
            RedoCommand.RaiseCanExecuteChanged();
            ResetCommand.RaiseCanExecuteChanged();
            MoveUpItemsCommand.RaiseCanExecuteChanged();
            MoveTopItemsCommand.RaiseCanExecuteChanged();
            MoveDownItemsCommand.RaiseCanExecuteChanged();
            MoveBottomItemsCommand.RaiseCanExecuteChanged();
            ReplaceItemsCommand.RaiseCanExecuteChanged();
            CollectItemsCommand.RaiseCanExecuteChanged();
            CurrentItem?.ChangeTracker?.RaiseCommandsCanExecute();
        }

        protected virtual void CurrentItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaiseCommandsCanExecute();
        }

        private void RemovedItemsList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    e.NewItems.Cast<TData>().ToList().ForEach(o => o.IsRemoved = true);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    e.OldItems.Cast<TData>().ToList().ForEach(o => o.IsRemoved = false);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
                case NotifyCollectionChangedAction.Reset:
                    break;
                default:
                    break;
            }
        }

        private bool ItemsHaveChanges()
        {
            return Source.FirstOrDefault(o => o.ChangeTracker.HasChanges) != null;
        }

        protected void ResetItemChanges()
        {
            foreach (TData item in Source)
                item.ChangeTracker.Reset();
        }

        protected void ClearPropertyChanges()
        {
            foreach (var item in Source.Where(o => o.ChangeTracker.HasChanges))
                item.ChangeTracker.ClearChanges();
        }

        protected override void ClearChangesOverride()
        {
            Source.Where(o => o.IsNew).ToList().ForEach(o => o.IsNew = false);
            for (int i = RemovedItemsList.Count - 1; i >= 0; i--)
                RemovedItemsList.RemoveAt(i);
        }

        private bool CanClearItems(object args)
        {
            return !(HasChanges || !ItemsHaveChanges());
        }

        private void ClearItems(object args)
        {
            CurrentItem = null;
            Source.Clear();
        }

        private bool CanRemoveItems(object args)
        {
            List<TData> items = (args as IEnumerable)?.Cast<TData>().ToList();
            return items != null && items.Count > 0;
        }

        private void RemoveItems(object args)
        {
            List<TData> items = (((IEnumerable)args).Cast<TData>()).ToList();
            IEnumerable<int> indexes = Source.GetItemIndexes(items).ToList();
            TData item;
            foreach (int i in indexes.OrderByDescending(o => o))
            {
                item = Source[i];
                Source.RemoveAt(i);
                RemovedItemsList.Insert(0, item);
            }

            int idx = indexes.Last();
            idx = Math.Min(Source.Count - 1, idx);

            CurrentItem = Source.ElementAtOrDefault(idx);
            RaiseCommandsCanExecute();
        }

        private bool CanUndo(object args)
        {
            return HasUndos;
        }

        private void Undo(object args)
        {
            NotifyCollectionChangedEventArgs change = base.Undo();
            int idx = -1;
            switch (change.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    idx = Math.Min(change.NewStartingIndex, Source.Count - 1);
                    foreach (TData item in change.NewItems.Cast<TData>().ToList())
                        RemovedItemsList.Insert(0, item);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    idx = Math.Min(change.OldStartingIndex, Source.Count - 1);
                    foreach (TData item in change.OldItems.Cast<TData>().ToList())
                        RemovedItemsList.Remove(item);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    break;
                case NotifyCollectionChangedAction.Move:
                    idx = change.OldStartingIndex;
                    break;
                case NotifyCollectionChangedAction.Reset:
                    break;
                default:
                    break;
            }

            CurrentItem = Source.ElementAtOrDefault(idx);
            RaiseCommandsCanExecute();
            Debug.WriteLine($"Undo: idx={idx}");
        }

        private bool CanRedo(object args)
        {
            return HasRedos;
        }

        private void Redo(object args)
        {
            NotifyCollectionChangedEventArgs change = base.Redo();
            int idx = -1;
            switch (change.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    idx = Math.Min(change.NewStartingIndex, Source.Count - 1);
                    foreach (TData item in change.NewItems.Cast<TData>().ToList())
                        RemovedItemsList.Remove(item);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    idx = Math.Min(change.OldStartingIndex, Source.Count - 1);
                    foreach (TData item in change.OldItems.Cast<TData>().ToList())
                        RemovedItemsList.Insert(0, item);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    break;
                case NotifyCollectionChangedAction.Move:
                    idx = change.NewStartingIndex;
                    break;
                case NotifyCollectionChangedAction.Reset:
                    break;
                default:
                    break;
            }

            CurrentItem = Source.ElementAtOrDefault(idx);
            RaiseCommandsCanExecute();
            Debug.WriteLine($"Redo: idx={idx}");
        }

        private bool CanReset(object args)
        {
            return HasUndos || HasRedos;
        }

        private void Reset(object args)
        {
            ResetItemChanges();
            base.Reset();
            foreach (TData item in Source.Where(o => o.IsRemoved))
                item.IsRemoved = false;

            CurrentItem = Source.ElementAtOrDefault(0);
            RaiseCommandsCanExecute();
        }

        private bool CanMoveUpItems(object args)
        {
            return Source.CanMoveUpItems((args as IEnumerable)?.Cast<TData>());
        }

        private void MoveUpItems(object args)
        {
            List<TData> items = (((IEnumerable)args).Cast<TData>()).ToList();
            Source.MoveUpItems(items);
            RaiseCommandsCanExecute();
        }

        private bool CanMoveTopItems(object args)
        {
            return Source.CanMoveTopItems((args as IEnumerable)?.Cast<TData>());
        }

        private void MoveTopItems(object args)
        {
            List<TData> items = (((IEnumerable)args).Cast<TData>()).ToList();
            Source.MoveTopItems(items);
            RaiseCommandsCanExecute();
        }

        private bool CanMoveDownItems(object args)
        {
            return Source.CanMoveDownItems((args as IEnumerable)?.Cast<TData>());
        }

        private void MoveDownItems(object args)
        {
            List<TData> items = (((IEnumerable)args).Cast<TData>()).ToList();
            Source.MoveDownItems(items);
            RaiseCommandsCanExecute();
        }

        private bool CanMoveBottomItems(object args)
        {
            return Source.CanMoveBottomItems((args as IEnumerable)?.Cast<TData>());
        }

        private void MoveBottomItems(object args)
        {
            List<TData> items = (((IEnumerable)args).Cast<TData>()).ToList();
            Source.MoveBottomItems(items);
            RaiseCommandsCanExecute();
        }

        private bool CanReplaceItems(object args)
        {
            return Source.CanReplaceItems((args as IEnumerable)?.Cast<TData>());
        }

        private void ReplaceItems(object args)
        {
            List<TData> items = (((IEnumerable)args).Cast<TData>()).ToList();
            Source.ReplaceItems(items);
            RaiseCommandsCanExecute();
        }

        private bool CanCollectItems(object args)
        {
            return Source.CanCollectItems((args as IEnumerable)?.Cast<TData>());
        }

        private void CollectItems(object args)
        {
            List<TData> items = (((IEnumerable)args).Cast<TData>()).ToList();
            Source.CollectItems(items);
            RaiseCommandsCanExecute();
        }
    }
}
