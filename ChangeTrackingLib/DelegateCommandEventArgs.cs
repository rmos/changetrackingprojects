﻿using System;

namespace ChangeTracking
{
    public class DelegateCommandEventArgs : EventArgs
    {
        public DelegateCommand Command { get; private set; }

        public DelegateCommandEventArgs(DelegateCommand command)
        {
            Command = command;
        }
    }
}
