﻿using LinqDataView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace ChangeTracking
{
    public abstract class DataTrackingContextBase<T> : CollectionTrackingContext<ObservableCollection<T>, T>
        where T : class, ITrackingObject
    {
        public event EventHandler<DelegateCommandEventArgs> CommandExecuted;

        #region Commands

        public DelegateCommand LoadDataCommand { get; private set; }

        public DelegateCommand LoadDataAsyncCommand { get; private set; }

        public DelegateCommand ClearDataCommand { get; private set; }

        public DelegateCommand CreateItemCommand { get; private set; }

        public DelegateCommand UpdateDataCommand { get; private set; }

        public DelegateCommand ResetFilterCommand { get; private set; }

        #endregion

        public abstract IQueryable<T> DataSource { get; }

        public abstract DataViewBase<T> ItemsView { get; }

        protected DataTrackingContextBase() : this(new ObservableCollection<T>())
        {
        }

        protected DataTrackingContextBase(ObservableCollection<T> source) : base(source)
        {
            CreateCommands();
        }

        private void CreateCommands()
        {
            LoadDataCommand = new DelegateCommand(LoadData, CanLoadData);
            LoadDataAsyncCommand = new DelegateCommand(LoadDataAsync, CanLoadData);
            ClearDataCommand = new DelegateCommand(ClearData, CanClearData);
            CreateItemCommand = new DelegateCommand(CreateItem, CanCreateItem);
            UpdateDataCommand = new DelegateCommand(UpdateData, CanUpdateData);
            ResetFilterCommand = new DelegateCommand(ResetFilter, CanResetFilter);
        }

        public override void RaiseCommandsCanExecute()
        {
            base.RaiseCommandsCanExecute();
            LoadDataCommand.RaiseCanExecuteChanged();
            LoadDataAsyncCommand.RaiseCanExecuteChanged();
            ClearDataCommand.RaiseCanExecuteChanged();
            CreateItemCommand.RaiseCanExecuteChanged();
            UpdateDataCommand.RaiseCanExecuteChanged();
            ResetFilterCommand.RaiseCanExecuteChanged();
        }

        protected void OnCommandExecuted(DelegateCommand command)
        {
            EventHandler<DelegateCommandEventArgs> h = CommandExecuted;
            h?.Invoke(this, new DelegateCommandEventArgs(command));
        }

        protected virtual bool CanCreateItem(object args)
        {
            return true;
        }

        protected abstract T CreateItemOverride(object args);

        public void CreateItem(object args)
        {
            if (!IsTracking)
                StartTracking();
            int idx = CurrentItem == null ? 0 : Source.IndexOf(CurrentItem);
            T item = CreateItemOverride(args);
            item.IsNew = true;
            Source.Insert(idx, item);
            CurrentItem = item;
            RaiseCommandsCanExecute();
            OnCommandExecuted(CreateItemCommand);
        }

        protected bool ItemsHaveChanges()
        {
            bool result = false;

            foreach (var item in Source)
            {
                if (item.ChangeTracker.HasChanges)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        protected virtual bool CanLoadData(object args)
        {
            return true;
        }

        protected abstract Task<List<T>> LoadDataAsyncOverride(IQueryable<T> dataViewResult);

        public async void LoadDataAsync(object args)
        {
            List<T> list = await LoadDataAsyncOverride(ItemsView.Apply(DataSource));
            ProcessLoadData(list);
        }

        public void LoadData(object args)
        {
            List<T> list = ItemsView.Apply(DataSource).ToList();
            ProcessLoadData(list);
        }

        private void ProcessLoadData(List<T> items)
        {
            if (IsTracking)
                StopTracking();
            ClearChanges();
            Source.Clear();

            foreach (var obj in items)
                Source.Add(obj);

            CurrentItem = Source.ElementAtOrDefault(0);

            StartTracking();
            RaiseCommandsCanExecute();
            OnCommandExecuted(LoadDataCommand);
        }

        protected virtual bool CanClearData(object args)
        {
            return Source.Count > 0 && !(HasChanges || ItemsHaveChanges());
        }

        public void ClearData(object args)
        {
            if (IsTracking)
                StopTracking();

            ClearChanges();
            Source.Clear();

            RaiseCommandsCanExecute();
            OnCommandExecuted(ClearDataCommand);
        }

        protected virtual bool CanUpdateData(object args)
        {
            return RemovedItems.Where(o => !o.IsNew).Union(Source.Where(o => o.ChangeTracker.HasChanges || o.IsNew)).Distinct().Count() > 0;
        }

        protected abstract void UpdateDataOverride(IEnumerable<T> added, IEnumerable<T> deleted);

        public void UpdateData(object args)
        {
            if (IsTracking)
                StopTracking();
            UpdateDataOverride(Source.Where(o => o.IsNew).ToList(), RemovedItems.Where(o => !o.IsNew).ToList());
            ClearPropertyChanges();
            ClearChanges();
            RaiseCommandsCanExecute();
            OnCommandExecuted(UpdateDataCommand);
        }

        protected virtual bool CanResetFilter(object args)
        {
            return !ItemsView.IsEmpty;
        }

        private void ResetFilter(object args)
        {
            ItemsView.Reset();
            RaiseCommandsCanExecute();
            OnCommandExecuted(ResetFilterCommand);
        }
    }
}
