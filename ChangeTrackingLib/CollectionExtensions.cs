﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ChangeTracking
{
    public static class CollectionExtensions
    {
        private static bool ValidateArgs<T>(IList<T> source, IEnumerable<T> items)
        {
            if (items == null || source.Count == 0)
                return false;

            int count = items.Count();
            return count > 0 && count <= source.Count;
        }

        public static IEnumerable<int> GetItemIndexes<T>(this IList<T> source, IEnumerable<T> items)
        {
            foreach (var item in items)
                yield return source.IndexOf(item);
        }

        public static int GetMinIndex<T>(this IList<T> source, IEnumerable<T> items)
        {
            return source.GetItemIndexes(items).Min();
        }

        public static int GetMaxIndex<T>(this IList<T> source, IEnumerable<T> items)
        {
            return source.GetItemIndexes(items).Max();
        }

        public static bool AreSuccessiveItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            var list = source.GetItemIndexes(items).OrderBy(o => o).ToList();
            bool result = list.Count > 0;

            for (int i = 1; i < list.Count; i++)
            {
                if (list[i] - 1 != list[i - 1])
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public static void MoveItem<T>(this IList<T> source, int oldIndex, int newIndex)
        {
            if (oldIndex == newIndex)
                return;

            if (source is ObservableCollection<T>)
                ((ObservableCollection<T>)source).MoveItem(oldIndex, newIndex);
            else
            {
                T item = source[oldIndex];
                source.RemoveAt(oldIndex);
                source.Insert(newIndex, item);
            }
        }

        public static void MoveItem<T>(this ObservableCollection<T> source, int oldIndex, int newIndex)
        {
            if (oldIndex == newIndex)
                return;

            source.Move(oldIndex, newIndex);
        }

        public static bool CanMoveUpItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            return ValidateArgs(source, items) && items.Min(o => source.IndexOf(o)) > 0;
        }

        public static void MoveUpItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            var enr = items.OrderBy(o => source.IndexOf(o)).ToList().GetEnumerator();
            int idx;
            while (enr.MoveNext())
            {
                idx = source.IndexOf(enr.Current);
                source.MoveItem(idx, idx - 1);
            }
        }

        public static bool CanMoveDownItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            return ValidateArgs(source, items) && items.Max(o => source.IndexOf(o)) < source.Count - 1;
        }

        public static void MoveDownItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            var enr = items.OrderByDescending(o => source.IndexOf(o)).ToList().GetEnumerator();
            int idx;
            while (enr.MoveNext())
            {
                idx = source.IndexOf(enr.Current);
                source.MoveItem(idx, idx + 1);
            }
        }

        public static bool CanMoveTopItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            if (ValidateArgs(source, items))
            {
                int min = items.Min(o => source.IndexOf(o));
                return min > 0 || !source.AreSuccessiveItems(items);
            }

            return false;
        }

        public static void MoveTopItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            var enr = items.OrderBy(o => source.IndexOf(o)).ToList().GetEnumerator();
            int idx = 0;
            int idx2;
            while (enr.MoveNext())
            {
                idx2 = source.IndexOf(enr.Current);
                source.MoveItem(idx2, idx);
                idx++;
            }
        }

        public static bool CanMoveBottomItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            if (ValidateArgs(source, items))
            {
                int max = items.Max(o => source.IndexOf(o));
                return max < source.Count - 1 || !source.AreSuccessiveItems(items);
            }

            return false;
        }

        public static void MoveBottomItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            var enr = items.OrderBy(o => source.IndexOf(o)).ToList().GetEnumerator();
            int idx, idx2 = source.Count - 1;
            while (enr.MoveNext())
            {
                idx = source.IndexOf(enr.Current);
                source.MoveItem(idx, idx2);
            }
        }

        public static bool CanReplaceItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            return ValidateArgs(source, items) && items.Count() == 2;
        }

        public static void ReplaceItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            List<T> list = items.ToList();
            int idx = source.IndexOf(list[0]);
            int idx2 = source.IndexOf(list[1]);
            source.MoveItem(idx, idx2);
            idx2 = source.IndexOf(list[1]);
            source.MoveItem(idx2, idx);
        }

        public static bool CanCollectItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            return ValidateArgs(source, items) && items.Count() > 1 && !source.AreSuccessiveItems(items);
        }

        public static void CollectItems<T>(this IList<T> source, IEnumerable<T> items)
        {
            var enr = items.OrderBy(o => source.IndexOf(o)).ToList().GetEnumerator();

            if (enr.MoveNext())
            {
                int idx = source.IndexOf(enr.Current);
                int idx2;
                while (enr.MoveNext())
                {
                    idx2 = source.IndexOf(enr.Current);
                    idx++;
                    source.MoveItem(idx2, idx);
                }
            }
        }
    }
}