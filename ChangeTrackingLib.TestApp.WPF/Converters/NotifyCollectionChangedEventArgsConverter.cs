﻿using ChangeTracking.TestApp.WPF.Extensions;
using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Windows.Data;

namespace ChangeTracking.TestApp.WPF.Converters
{
    public class NotifyCollectionChangedEventArgsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((NotifyCollectionChangedEventArgs)value)?.ToString2();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
