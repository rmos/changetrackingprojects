﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ChangeTracking.TestApp.WPF.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool? val = value as bool?;
            return val.HasValue ? val.Value ? Visibility.Visible : Visibility.Hidden : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string val = value as string;
            return string.IsNullOrWhiteSpace(val) ? Visibility.Hidden : string.Compare("true", val, true) == 0 ? Visibility.Visible : Visibility.Hidden;
        }
    }
}
