﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using LinqDataView;

namespace ChangeTracking.TestApp.WPF.Models
{
    public class DescriptorDataView : DataViewBase<Descriptor>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string title;

        public string Title
        {
            get { return title; }
            set
            {
                if (value != title)
                {
                    title = value;
                    OnPropertyChanged(nameof(Title));
                    OnPropertyChanged(nameof(IsEmpty));
                }
            }
        }

        private bool isTitleContains;

        public bool IsTitleContains
        {
            get { return isTitleContains; }
            set
            {
                if (value != isTitleContains)
                {
                    isTitleContains = value;
                    OnPropertyChanged(nameof(IsTitleContains));
                }
            }
        }


        private string description;

        public string Description
        {
            get { return description; }
            set
            {
                if (value != description)
                {
                    description = value;
                    OnPropertyChanged(nameof(Description));
                    OnPropertyChanged(nameof(IsEmpty));
                }
            }
        }

        private bool isDescriptionContains;

        public bool IsDescriptionContains
        {
            get { return isDescriptionContains; }
            set
            {
                if (value != isDescriptionContains)
                {
                    isDescriptionContains = value;
                    OnPropertyChanged(nameof(IsDescriptionContains));
                }
            }
        }

        public override bool IsEmpty
        {
            get
            {
                return string.IsNullOrWhiteSpace(Title)
                    && string.IsNullOrWhiteSpace(Description);
            }
        }

        public override void ResetFilter()
        {
            Title = null;
            Description = null;
        }

        protected override IQueryable<Descriptor> ApplyFilter(IQueryable<Descriptor> items)
        {
            IQueryable<Descriptor> result = items;

            string val = Title;
            if (!string.IsNullOrWhiteSpace(Title))
            {
                result = result.Where(o => IsTitleContains ? o.Title.Contains(val) : o.Title.StartsWith(val));
            }

            string val1 = Description;
            if (!string.IsNullOrWhiteSpace(Description))
            {
                result = result.Where(o => IsDescriptionContains ? o.Description.Contains(val1) : o.Description.StartsWith(val1));
            }

            return result;
        }
        
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler h = PropertyChanged;
            h?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected override void InitSorting(IDictionary<string, char> sort)
        {
            sort.Add(nameof(Title), DataViewBase<Descriptor>.Ascending);
        }
    }
}
