﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows;

namespace ChangeTracking.TestApp.WPF.Models
{
    public class ModelObjectBase : TrackingObject
    {
        [NotMapped]
        public new PropertyTrackingContext<INotifyPropertyChanged> ChangeTracker { get { return base.ChangeTracker; } }

        [NotMapped]
        public override bool IsNew { get => base.IsNew; set => base.IsNew = value; }

        [NotMapped]
        public override bool IsRemoved { get => base.IsRemoved; set => base.IsRemoved = value; }

        protected override void OnPropertyChanged(string propertyName)
        {
            Application.Current.Dispatcher.Invoke(() => base.OnPropertyChanged(propertyName));
        }
    }
}