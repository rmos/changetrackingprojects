﻿using ChangeTracking.TestApp.WPF.Properties;
using LinqDataView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ChangeTracking.TestApp.WPF.Models
{
    public class DescriptorContext : DataTrackingContextBase<Descriptor>, IDisposable
    {
        ChangeTrackingDatabase Database = new ChangeTrackingDatabase();

        public override IQueryable<Descriptor> DataSource
        {
            get
            {
                return Database.Descriptors;
            }
        }

        public int PageSize
        {
            get { return ItemsView.PageSize; }
            set
            {
                if (value != ItemsView.PageSize)
                {
                    ItemsView.PageSize = value;
                    Settings.Default.DescriptorContextPageSize = value;
                    OnPropertyChanged(nameof(PageSize));
                }
            }
        }

        public int PageNum
        {
            get { return ItemsView.PageNum; }
            set
            {
                if (value != ItemsView.PageNum)
                {
                    ItemsView.PageNum = value;
                    OnPropertyChanged(nameof(PageNum));
                }
            }
        }


        private DataViewBase<Descriptor> itemsView;
        public override DataViewBase<Descriptor> ItemsView
        {
            get
            {
                if (itemsView == null)
                {
                    DescriptorDataView view = new DescriptorDataView();
                    view.PropertyChanged += ItemsView_PropertyChanged;
                    itemsView = view;
                }

                return itemsView;
            }
        }

        private void ItemsView_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ResetFilterCommand.RaiseCanExecuteChanged();
        }

        private static int lastId;

        public int LastId
        {
            get { return lastId; }
            set
            {
                if (value != lastId)
                {
                    lastId = value;
                    OnPropertyChanged(nameof(LastId));
                }
            }
        }

        public DescriptorContext()
        {
            ItemsView.PageSize = Settings.Default.DescriptorContextPageSize;
            Settings.Default.PropertyChanged += Settings_Default_PropertyChanged;
        }

        private void Settings_Default_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Settings.Default.Save();
        }

        protected override Descriptor CreateItemOverride(object args)
        {
            LastId += 1;
            return new Descriptor { Title = $"Title{LastId}", Description = $"Descriptor{LastId}" };
        }

        protected async override Task<List<Descriptor>> LoadDataAsyncOverride(IQueryable<Descriptor> dataViewResult)
        {
            return await dataViewResult.ToListAsync();
        }

        protected async override void UpdateDataOverride(IEnumerable<Descriptor> added, IEnumerable<Descriptor> deleted)
        {
            Database.Descriptors.AddRange(added);
            Database.Descriptors.RemoveRange(deleted);

            int count = -1;
            if (Database.ChangeTracker.HasChanges())
                count = await Database.SaveChangesAsync();
            Debug.WriteLine($"Data updated: count={count}");
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Database.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~DescriptorContext()
        {
            Dispose(false);
        }
    }
}
