﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ChangeTracking.TestApp.WPF.Models
{
    [Table("Descriptor")]
    public class Descriptor : ModelObjectBase
    {
        private int id;
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return id; }
            set
            {
                if (value != id)
                {
                    id = value;
                    OnPropertyChanged(nameof(Id));
                }
            }
        }

        private string title;
        [ChangeTracking]
        public string Title
        {
            get { return title; }
            set
            {
                if (value != title)
                {
                    title = value;
                    OnPropertyChanged(nameof(Title));
                }
            }
        }

        private string description;
        [ChangeTracking]
        public string Description
        {
            get { return description; }
            set
            {
                if (value != description)
                {
                    description = value;
                    OnPropertyChanged(nameof(Description));
                }
            }
        }

        public override string ToString()
        {
            return $"Title: {Title};Description: {Description}";
        }
    }
}
