﻿using System.Data.Entity;

namespace ChangeTracking.TestApp.WPF.Models
{
    public class ChangeTrackingDatabase : DbContext
    {
        public DbSet<Descriptor> Descriptors { get; set; }
    }
}
