﻿using ChangeTracking.TestApp.WPF.Models;
using System.Windows;
using System.Windows.Controls;

namespace ChangeTracking.TestApp.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public new DescriptorContext DataContext
        {
            get
            {
                return base.DataContext as DescriptorContext;
            }

            set
            {
                base.DataContext = value as DescriptorContext;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext.CommandExecuted += DataContext_CommandExecuted;
        }

        private void DataContext_CommandExecuted(object sender, DelegateCommandEventArgs e)
        {
            //todo: revise
            foreach (var item in ItemSelector.SelectedItems)
                ItemSelector.ScrollIntoView(item);
        }

        private void ItemSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataContext?.RaiseCommandsCanExecute();
        }

        private void ChangeSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ChangeSelector.SelectedItem != null)
                ChangeSelector.ScrollIntoView(ChangeSelector.SelectedItem);
        }
    }
}
