﻿using System.Collections.Specialized;

namespace ChangeTracking.TestApp.WPF.Extensions
{
    public static class Extensions
    {
        public static string ToString2(this NotifyCollectionChangedEventArgs item)
        {
            return $"{item.Action}: NewItems={item.NewItems?.Count} OldItems={item.OldItems?.Count} NewIdx={item.NewStartingIndex} OldIdx={item.OldStartingIndex}";
        }
    }
}
